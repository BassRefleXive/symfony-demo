<?php


namespace Printify\SymfonyDemo\Tests\Domain\User\Service;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Printify\SymfonyDemo\Domain\User\Model\Address;
use Printify\SymfonyDemo\Domain\User\Model\User;
use Printify\SymfonyDemo\Domain\User\Repository\UserRepositoryInterface;
use Printify\SymfonyDemo\Domain\User\Service\UserService;

class UserServiceTest extends TestCase
{
    /**
     * @var UserRepositoryInterface|MockObject
     */
    private $repository;

    /**
     * @var UserService
     */
    private $service;

    protected function setUp(): void
    {
        $this->repository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $this->service = new UserService($this->repository);
    }

    /**
     * @test
     */
    public function itCreateNewUser(): void
    {
        $this->repository
            ->expects($this->once())
            ->method('save')
            ->with(
                $this->callback(function (User $user): bool {
                    $this->assertSame('a', $user->firstName());
                    $this->assertSame('b', $user->lastName());
                    $this->assertSame('c', $user->email());
                    $this->assertSame('d', $user->password());

                    return true;
                })
            );

        $this->service->createUser('a', 'b', 'c', 'd');
    }

    /**
     * @test
     */
    public function itAddNewAddress(): void
    {
        // It is possible to mock models
        $user = $this->createMock(User::class);

        $user->expects($this->once())
            ->method('addAddress')
            ->with(
                $this->callback(function (Address $address): bool {
                    $this->assertSame('a', $address->street());
                    $this->assertSame(123, $address->buildingNumber());
                    $this->assertSame(234, $address->apartment());
                    $this->assertSame('b', $address->postalCode());

                    return true;
                })
            );

        // It is possible to tell repository mock to return mocked user
        $this->repository
            ->expects($this->once())
            ->method('getById')
            ->with(
                $this->identicalTo('c')
            )
            ->willReturn($user);

        $this->repository
            ->expects($this->once())
            ->method('save')
            ->with(
                $this->identicalTo($user)
            );

        $this->service->addUserAddress('c', 'a', 123, 234, 'b');
    }
}