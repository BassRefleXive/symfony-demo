<?php

declare(strict_types=1);

namespace Printify\SymfonyDemo\Infrastructure\User\Repository;


use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Printify\SymfonyDemo\Domain\User\Model\User;
use Printify\SymfonyDemo\Domain\User\Repository\UserRepositoryInterface;

/**
 * @link Query Builder API: https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/1.2/reference/query-builder-api.html
 */
class UserRepository extends ServiceDocumentRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $user): void
    {
        $this->getDocumentManager()->persist($user);
        $this->getDocumentManager()->flush($user);
    }

    public function getById(string $userId): User
    {
        $qb = $this->createQueryBuilder();

        return $qb->select()
            ->field('id')->equals($userId)
            ->getQuery()
            ->getSingleResult();
    }

    public function all(): array
    {
        return $this->findAll();
    }
}