<?php


namespace Printify\SymfonyDemo\Application\Command;


use JMS\Serializer\SerializerInterface;
use Printify\SymfonyDemo\Domain\User\Service\UserServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddAddressCommand extends Command
{
    private $service;
    private $serializer;

    public function __construct(UserServiceInterface $service, SerializerInterface $serializer)
    {
        parent::__construct();

        $this->service = $service;
        $this->serializer = $serializer;
    }

    public function configure(): void
    {
        $this
            ->setName('printify:symfony-demo:user:add-address')
            ->addArgument('user-id', InputArgument::REQUIRED)
            ->addArgument('street', InputArgument::REQUIRED)
            ->addArgument('building', InputArgument::REQUIRED)
            ->addArgument('apartment', InputArgument::REQUIRED)
            ->addArgument('postal-code', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Add User Address');

        $userId = $input->getArgument('user-id');
        $street = $input->getArgument('street');
        $building = (int)$input->getArgument('building');
        $apartment = (int)$input->getArgument('apartment');
        $postalCode = $input->getArgument('postal-code');

        $user = $this->service->addUserAddress($userId, $street, $building, $apartment, $postalCode);

        $output->writeln(
            $this->serializer->serialize(
                $user,
                'json'
            )
        );
    }
}