<?php

declare(strict_types=1);

namespace Printify\SymfonyDemo\Application\Command;


use Printify\SymfonyDemo\Domain\User\Service\UserServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    private $service;

    public function __construct(UserServiceInterface $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function configure(): void
    {
        $this
            ->setName('printify:symfony-demo:user:create')
            ->addArgument('first-name', InputArgument::REQUIRED)
            ->addArgument('last-name', InputArgument::REQUIRED)
            ->addOption('email', null, InputOption::VALUE_REQUIRED)
            ->addOption('password', null, InputOption::VALUE_REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Create User');

        $firstName = $input->getArgument('first-name');
        $lastName = $input->getArgument('last-name');
        $email = $input->getOption('email');
        $password = $input->getOption('password');

        $user = $this->service->createUser($firstName, $lastName, $email, $password);

        $output->writeln(sprintf('ID: %s', $user->id()));
        $output->writeln(sprintf('First Name: %s', $user->firstName()));
        $output->writeln(sprintf('Last Name: %s', $user->lastName()));
        $output->writeln(sprintf('Email: %s', $user->email()));
        $output->writeln(sprintf('Password: %s', $user->password()));
    }
}