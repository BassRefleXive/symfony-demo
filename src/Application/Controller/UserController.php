<?php


namespace Printify\SymfonyDemo\Application\Controller;


use JMS\Serializer\SerializerInterface;
use Printify\SymfonyDemo\Domain\User\Repository\UserRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/api/user")
 */
class UserController
{
    /**
     * @Route(methods={"GET"}, path="/get")
     */
    public function all(UserRepositoryInterface $userRepository, SerializerInterface $serializer): Response
    {
        return new Response(
            $serializer->serialize(
                $userRepository->all(),
                'json'
            ),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json',
            ]
        );
    }
}