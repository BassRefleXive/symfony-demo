<?php


namespace Printify\SymfonyDemo\Domain\User\Service;

use Printify\SymfonyDemo\Domain\User\Model\User;
use Psr\Log\LoggerInterface;

class UserServiceDecoratorB implements UserServiceInterface
{
    private $service;
    private $logger;
    private $exampleDependency;

    public function __construct(UserServiceInterface $service, LoggerInterface $logger, string $exampleDependency)
    {
        $this->service = $service;
        $this->logger = $logger;
        $this->exampleDependency = $exampleDependency;
    }

    public function createUser(string $firstName, string $lastName, string $email, string $password): User
    {
        $this->logger->info(
            '{class}::createUser, injected example dependency: {dependency}.',
            [
                'class' => get_class($this),
                'dependency' => $this->exampleDependency,
            ]
        );

        return $this->service->createUser($firstName, $lastName, $email, $password);
    }

    public function addUserAddress(string $userId, string $street, int $buildingNumber, int $apartment, string $postalCode): User
    {
        $this->logger->info(
            '{class}::addUserAddress, injected example dependency: {dependency}.',
            [
                'class' => get_class($this),
                'dependency' => $this->exampleDependency,
            ]
        );

        return $this->service->addUserAddress($userId, $street, $buildingNumber, $apartment, $postalCode);
    }
}