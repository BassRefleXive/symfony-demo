<?php

declare(strict_types=1);

namespace Printify\SymfonyDemo\Domain\User\Service;


use Printify\SymfonyDemo\Domain\User\Model\Address;
use Printify\SymfonyDemo\Domain\User\Model\User;
use Printify\SymfonyDemo\Domain\User\Repository\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function createUser(string $firstName, string $lastName, string $email, string $password): User
    {
        $user = new User($firstName, $lastName, $email, $password);

        $this->repository->save($user);

        return $user;
    }

    public function addUserAddress(string $userId, string $street, int $buildingNumber, int $apartment, string $postalCode): User
    {
        $user = $this->repository->getById($userId);

        $user->addAddress(
            new Address(
                $street,
                $buildingNumber,
                $apartment,
                $postalCode
            )
        );

        $this->repository->save($user);

        return $user;
    }
}