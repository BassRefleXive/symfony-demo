<?php


namespace Printify\SymfonyDemo\Domain\User\Service;


use Printify\SymfonyDemo\Domain\User\Model\User;

interface UserServiceInterface
{
    public function createUser(string $firstName, string $lastName, string $email, string $password): User;

    public function addUserAddress(string $userId, string $street, int $buildingNumber, int $apartment, string $postalCode): User;
}