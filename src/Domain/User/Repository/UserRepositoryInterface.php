<?php

declare(strict_types=1);

namespace Printify\SymfonyDemo\Domain\User\Repository;


use Printify\SymfonyDemo\Domain\User\Model\User;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function getById(string $userId): User;

    public function all(): array;
}