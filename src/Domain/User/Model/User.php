<?php

declare(strict_types=1);

namespace Printify\SymfonyDemo\Domain\User\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as JMS;


/**
 * @link Basic mapping - https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/1.2/reference/basic-mapping.html
 *
 * @MongoDB\Document(collection="User", repositoryClass="\Printify\SymfonyDemo\Domain\User\Repository\UserRepositoryInterface")
 *
 * @JMS\ExclusionPolicy("all")
 */
class User
{
    /**
     * @MongoDB\Id
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $firstName;

    /**
     * @MongoDB\Field(type="string")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $lastName;

    /**
     * @MongoDB\Field(type="string")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @MongoDB\Field(type="string")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $password;

    /**
     * @MongoDB\EmbedMany(targetDocument="Printify\SymfonyDemo\Domain\User\Model\Address")
     *
     * @link Embedded documents mapping - https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/1.2/reference/embedded-mapping.html
     *
     * @JMS\Expose()
     * @JMS\Type("ArrayCollection<Printify\SymfonyDemo\Domain\User\Model\Address>")
     */
    private $addresses;

    public function __construct(string $firstName, string $lastName, string $email, string $password)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function addresses(): Collection
    {
        if (null === $this->addresses) {
            $this->addresses = new ArrayCollection();
        }

        return $this->addresses;
    }

    public function addAddress(Address $address): void
    {
        $this->addresses()->add($address);
    }
}