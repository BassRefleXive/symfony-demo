<?php


namespace Printify\SymfonyDemo\Domain\User\Model;


use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as JMS;

/**
 * @MongoDB\EmbeddedDocument()
 *
 * @JMS\ExclusionPolicy("all")
 */
class Address
{
    /**
     * @MongoDB\Field(type="string", name="street_name")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $street;

    /**
     * @MongoDB\Field(type="int", name="building_number")
     *
     * @JMS\Expose()
     * @JMS\Type("int")
     */
    private $buildingNumber;

    /**
     * @MongoDB\Field(type="int", name="apartment")
     *
     * @JMS\Expose()
     * @JMS\Type("int")
     */
    private $apartment;

    /**
     * @MongoDB\Field(type="string", name="postal_code")
     *
     * @JMS\Expose()
     * @JMS\Type("string")
     */
    private $postalCode;

    public function __construct(string $street, int $buildingNumber, int $apartment, string $postalCode)
    {
        $this->street = $street;
        $this->buildingNumber = $buildingNumber;
        $this->apartment = $apartment;
        $this->postalCode = $postalCode;
    }

    public function street(): string
    {
        return $this->street;
    }

    public function buildingNumber(): int
    {
        return $this->buildingNumber;
    }

    public function apartment(): int
    {
        return $this->apartment;
    }

    public function postalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @JMS\SerializedName("fill_address")
     * @JMS\VirtualProperty()
     */
    public function fullAddress(): string
    {
        return sprintf(
            '%s %d-%d, %s.',
            $this->street(),
            $this->buildingNumber(),
            $this->apartment(),
            $this->postalCode()
        );
    }
}